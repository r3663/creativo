# Overview

## Development

-   DevTools for the Win
-   console.log in the reducer
-   Encapsulate state
-   State from generated mocks
-   Services with this mocked data
-   Type interference
-   Small components
-   react persist for debug
-   Vim mode Feature
-   Encapsulated States
-   The first one: One big state
-   The Second one: The options, the user actions
-   The State and why it is typed like it is
-   Legacy Reducer
