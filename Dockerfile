# install node_modules used for caching
FROM node as deps

WORKDIR /app
COPY package.json /app
COPY package-lock.json /app

RUN npm install

FROM node as builder
ENV NODE_ENV production

WORKDIR /app
COPY . /app
COPY --from=deps /app/node_modules /app/node_modules

RUN npm run build

# run container
FROM nginx:latest

WORKDIR /app
COPY . /app
COPY --from=builder /app/build /usr/share/nginx/html/

CMD [ "nginx", "-g", "daemon off;" ]
