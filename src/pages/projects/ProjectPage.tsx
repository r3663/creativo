import React, { Dispatch } from 'react'
import {
    IonHeader,
    IonToolbar,
    IonPage,
    IonTitle,
    IonContent,
    IonLabel,
    IonSegment,
    IonSegmentButton,
    IonButtons,
    IonBackButton,
} from '@ionic/react'
import './ProjectPage.css'
import { CREATE_VIEW } from '../../types'
import { AppState, UserInteractions, updateUserSelection } from '../../store'
import { Create, Review } from './Components'
import { useSelector, useDispatch } from 'react-redux'
import './styles/layout.css'

export const ProjectPage: React.FC = () => {
    const { projects } = useSelector((state: AppState) => state.interaction)
    const setRedux = useDispatch<Dispatch<Partial<UserInteractions>>>()

    if (!projects) {
        return <div> no projects </div>
    }

    const render = () => {
        if (!projects) {
            return <h1> State not populated yet </h1>
        }
        if (!projects.create) {
            return <h1> State not populated yet </h1>
        }
        switch (projects.create.view) {
            case CREATE_VIEW.REVIEW:
                return <Review />
            case CREATE_VIEW.CREATE:
            default:
                return <Create />
        }
    }

    return (
        <IonPage>
            <IonHeader className="header">
                <img src="./assets/model.jpeg" alt="name" />
                <IonToolbar className="toolbar">
                    <IonButtons slot="start">
                        <IonBackButton
                            className="buttons"
                            defaultHref="/home"
                            icon={'arrow-back'}
                        />
                    </IonButtons>
                    <IonTitle>Projects</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <div
                    style={{
                        padding: '16px',
                    }}
                >
                    <IonSegment
                        value={
                            (projects.create && projects.create.view) ||
                            'create'
                        }
                        onIonChange={e =>
                            setRedux(
                                updateUserSelection({
                                    projects: {
                                        create: {
                                            ...projects.create,
                                            view: e.detail.value as any,
                                        },
                                    },
                                })
                            )
                        }
                    >
                        <IonSegmentButton value="create">
                            <IonLabel>Create</IonLabel>
                        </IonSegmentButton>
                        <IonSegmentButton value="review">
                            <IonLabel>Review</IonLabel>
                        </IonSegmentButton>
                    </IonSegment>
                    {render()}
                </div>
            </IonContent>
        </IonPage>
    )
}
