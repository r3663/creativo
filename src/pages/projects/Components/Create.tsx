import React, { Dispatch } from 'react'
import { AppState, UserInteractions, updateUserSelection } from '../../../store'

import {
    IonCard,
    IonSelect,
    IonSelectOption,
    IonList,
    IonListHeader,
    IonItem,
    IonInput,
} from '@ionic/react'
import { useSelector, useDispatch } from 'react-redux'

export const Create = () => {
    const setRedux = useDispatch<Dispatch<Partial<UserInteractions>>>()

    const interaction = useSelector((state: AppState) => state.interaction)
    const { app } = useSelector((state: AppState) => state.services)
    const user = useSelector((state: AppState) => state.user)

    if (!user) {
        return <div> navigate to login </div>
    }

    if (!user.projects) {
        return <div> no user project yet </div>
    }

    if (!interaction.profile) {
        return <div> return to login </div>
    }

    if (!interaction.projects) {
        return <div> return to login </div>
    }

    if (!interaction.projects.create) {
        return <div> return to login </div>
    }

    if (!interaction) {
        return <div> No projects </div>
    }

    if (!app) {
        return <div> No services </div>
    }

    return (
        <div>
            <IonCard>
                <IonSelect
                    placeholder="choose project"
                    onIonChange={e => {
                        if (!interaction.projects) {
                            return
                        }

                        const create = app.projects.filter(
                            p => p.id === e.detail.value
                        )[0]

                        setRedux(
                            updateUserSelection({
                                projects: {
                                    ...interaction.projects.create,
                                    create,
                                },
                            })
                        )
                    }}
                >
                    {user.projects.map(x => {
                        const userProject = app.projects.filter(
                            p => p.id === x
                        )[0]

                        return (
                            <IonSelectOption
                                key={userProject.id}
                                value={userProject.id}
                            >
                                {userProject.title}
                            </IonSelectOption>
                        )
                    })}
                </IonSelect>
            </IonCard>

            <IonList>
                <IonListHeader>General</IonListHeader>
                <IonItem>
                    <IonInput
                        placeholder="Title"
                        value={
                            interaction.projects &&
                            interaction.projects.create &&
                            interaction.projects.create.title
                        }
                        onIonChange={e => {
                            if (!interaction.projects) {
                                return
                            }

                            setRedux(
                                updateUserSelection({
                                    projects: {
                                        create: {
                                            ...interaction.projects.create,
                                            title: e.detail.value || '',
                                        },
                                    },
                                })
                            )
                        }}
                    ></IonInput>
                </IonItem>
                <IonItem>
                    <IonInput
                        placeholder="Description"
                        value={
                            interaction.projects &&
                            interaction.projects.create &&
                            interaction.projects.create.description
                        }
                        onIonChange={e => {
                            if (!interaction.projects) {
                                return
                            }

                            setRedux(
                                updateUserSelection({
                                    projects: {
                                        create: {
                                            ...interaction.projects.create,
                                            description: e.detail.value || '',
                                        },
                                    },
                                })
                            )
                        }}
                    ></IonInput>
                </IonItem>
            </IonList>
        </div>
    )

    // return (
    //     <div>
    //         <div>
    //         </div>
    //         <IonList>
    //             <IonListHeader>General</IonListHeader>
    //             <IonItem>
    //                 <IonInput
    //                     placeholder="Title"
    //                     value={
    //                         interaction.projects &&
    //                         interaction.projects.create &&
    //                         interaction.projects.create.title
    //                     }
    //                     onIonChange={e =>
    //                         setRedux(
    //                             updateUserSelection({
    //                                 projects: {
    //                                     create: {
    //                                         view: CREATE_VIEW.CREATE,
    //                                         title: e.detail.value || '',
    //                                     },
    //                                 },
    //                             })
    //                         )
    //                     }
    //                 ></IonInput>
    //             </IonItem>
    //             <IonItem>
    //                 <IonInput
    //                     placeholder="Description"
    //                     value={''}
    //                     onIonChange={e =>
    //                         setRedux(
    //                             updateUserSelection({
    //                                 projects: {
    //                                     create: {
    //                                         view: CREATE_VIEW.CREATE,
    //                                         description: e.detail.value || '',
    //                                     },
    //                                 },
    //                             })
    //                         )
    //                     }
    //                 ></IonInput>
    //             </IonItem>
    //             <IonListHeader>Date</IonListHeader>
    //             <IonItem>
    //                 <IonLabel>Start Date</IonLabel>
    //                 <IonDatetime
    //                     onIonChange={e =>
    //                         setRedux(
    //                             updateUserSelection({
    //                                 projects: {
    //                                     create: {
    //                                         view: CREATE_VIEW.CREATE,
    //                                         date: {
    //                                             start: e.detail.value,
    //                                         } as any,
    //                                     },
    //                                 },
    //                             })
    //                         )
    //                     }
    //                     placeholder="Start Date"
    //                 ></IonDatetime>
    //             </IonItem>
    //             <IonItem>
    //                 <IonLabel>Start Time</IonLabel>
    //                 <IonDatetime
    //                     onIonChange={e =>
    //                         setRedux(
    //                             updateUserSelection({
    //                                 projects: {
    //                                     create: {
    //                                         view: CREATE_VIEW.CREATE,
    //                                         date: {
    //                                             start: e.detail.value,
    //                                         } as any,
    //                                     },
    //                                 },
    //                             })
    //                         )
    //                     }
    //                     displayFormat="h:mm A"
    //                     pickerFormat="h:mm A"
    //                     placeholder="Start Time"
    //                 ></IonDatetime>
    //             </IonItem>
    //             <IonItem>
    //                 <IonLabel>End</IonLabel>
    //                 <IonDatetime
    //                     onIonChange={e =>
    //                         setRedux(
    //                             updateUserSelection({
    //                                 projects: {
    //                                     create: {
    //                                         view: CREATE_VIEW.CREATE,
    //                                         date: {
    //                                             end: e.detail.value,
    //                                         } as any,
    //                                     },
    //                                 },
    //                             })
    //                         )
    //                     }
    //                     placeholder="End"
    //                 ></IonDatetime>
    //             </IonItem>
    //             <IonListHeader>Explore</IonListHeader>
    //             <div className="project-page-category-gallery">
    //                 {locations.map(
    //                     (l: any) =>
    //                         l && (
    //                             <IonCard key={l.zipCode} className="card">
    //                                 <img src={'/assets/model.jpeg'} alt="" />
    //                                 <IonCardTitle color="light">
    //                                     {l.city}
    //                                 </IonCardTitle>
    //                             </IonCard>
    //                         )
    //                 )}
    //             </div>
    //         </IonList>
    //     </div>
    // )
}
