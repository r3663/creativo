import React from 'react'
import {
    IonCard,
    IonCardTitle,
    IonLabel,
    IonCardContent,
    IonCardSubtitle,
    IonChip,
    IonIcon,
} from '@ionic/react'
import { AppState } from '../../../store'
import { useSelector } from 'react-redux'

export const Review = () => {
    const { app } = useSelector((state: AppState) => state.services)
    const { projects } = useSelector((state: AppState) => state.interaction)

    if (!app) {
        return <div> No services </div>
    }
    if (!projects) {
        return <div> No projects </div>
    }

    const { create } = projects
    const { users, locations } = app

    if (!create) {
        return (
            <div> Start creating your special project somewhere else :D </div>
        )
    }

    return (
        <div>
            <IonCard>
                <IonCardContent className="padding">
                    <div>
                        <IonCardTitle>
                            <h1>{create.title}</h1>
                        </IonCardTitle>
                        <IonCardSubtitle>Description</IonCardSubtitle>
                        <p>{create.description}</p>
                    </div>
                    <IonCardSubtitle>User</IonCardSubtitle>
                    <div>
                        {create.users &&
                            create.users.map((u: any) => {
                                const us = users.filter((x: any) => x.id === u)
                                return us.map((chip: any) => (
                                    <IonChip key={chip.id}>
                                        <IonIcon name="heart" />
                                        <IonLabel>{chip.username}</IonLabel>
                                    </IonChip>
                                ))
                            })}
                    </div>
                    {create.date && (
                        <div>
                            <p>{create.date.start}</p>
                            <p>{create.date.end}</p>
                            <p>{create.date.id}</p>
                        </div>
                    )}
                </IonCardContent>

                <IonCardSubtitle>Locations</IonCardSubtitle>
                <p>
                    {create.locations &&
                        create.locations.map((u: any) => {
                            const us = locations.filter((x: any) => x.id === u)

                            return us.map((l: any) => (
                                <IonChip key={l.id}>
                                    <IonIcon name="heart" />
                                    <IonLabel>{l.city}</IonLabel>
                                </IonChip>
                            ))
                        })}
                </p>
            </IonCard>
        </div>
    )
}
