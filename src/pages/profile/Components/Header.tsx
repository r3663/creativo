import React from 'react'
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonCard,
    IonCardContent,
    IonCardTitle,
    IonCardSubtitle,
    IonButton,
} from '@ionic/react'

import './ProfilePage.css'

export const Header: React.FC = () => <IonContent>Header</IonContent>
