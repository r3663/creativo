import React from 'react'
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonCard,
    IonCardContent,
    IonCardTitle,
    IonCardSubtitle,
    IonButton,
} from '@ionic/react'

import './ProfilePage.css'
import { useSelector } from 'react-redux'
import { AppState } from '../../store'

export const ProfilePage: React.FC = () => {
    const profile = useSelector((state: AppState) => state.interaction.profile)
    const { app } = useSelector((state: AppState) => state.services)

    if (!app) {
        return <div> no services </div>
    }

    if (!profile) {
        return <div> goto Login </div>
    }

    const { projects } = app
    const { user } = profile

    if (!user) {
        return <div> goto Login</div>
    }

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonToolbar>
                        <IonTitle>Profile</IonTitle>
                    </IonToolbar>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonCard>
                    <IonCardContent className="padding">
                        <div className="profile-card-content">
                            <div className="avatar">
                                <img
                                    alt={`${user.username}-avatar`}
                                    src={user.avatar}
                                />
                            </div>

                            <div className="info">
                                <IonCardTitle>
                                    <h1> {user.username}</h1>
                                </IonCardTitle>
                                <IonCardSubtitle>
                                    <p>
                                        {user.firstName} - {user.lastName}
                                    </p>
                                    <p>{user.email}</p>
                                </IonCardSubtitle>
                            </div>
                        </div>
                    </IonCardContent>
                </IonCard>
                <IonCard>
                    <IonCardContent className="padding">
                        <div className="location-card-content">
                            {user.location ? (
                                <div className="card">
                                    <IonCardTitle>
                                        <h1>Location - {user.location.city}</h1>
                                    </IonCardTitle>
                                    <IonCardSubtitle>
                                        <div className="info">
                                            <p>
                                                {user.location.city} -{' '}
                                                {user.location.country} -{' '}
                                                {user.location.state}
                                            </p>
                                            <p
                                                style={{
                                                    marginTop: 12,
                                                }}
                                            >
                                                {user.location.streetAddress}
                                            </p>
                                            <p>{user.location.streetName}</p>
                                            <p>{user.location.zipCode}</p>
                                        </div>
                                    </IonCardSubtitle>
                                </div>
                            ) : (
                                <div className="card">
                                    <IonCardTitle>
                                        <h1> No Location</h1>
                                    </IonCardTitle>
                                    <IonCardSubtitle>
                                        <IonButton>Add</IonButton>
                                    </IonCardSubtitle>
                                </div>
                            )}
                        </div>
                    </IonCardContent>
                </IonCard>
                <IonCard href="/messages">
                    <IonCardContent className="padding">
                        <div className="conversations-card-content">
                            {user.conversations ? (
                                <div className="card">
                                    <IonCardTitle>
                                        <h1>Conversations </h1>
                                    </IonCardTitle>
                                    <IonCardSubtitle>
                                        <div className="info"></div>
                                    </IonCardSubtitle>
                                </div>
                            ) : (
                                <div className="card">
                                    <IonCardTitle>
                                        <h1> No Location</h1>
                                    </IonCardTitle>
                                    <IonCardSubtitle>
                                        <IonButton>Add</IonButton>
                                    </IonCardSubtitle>
                                </div>
                            )}
                        </div>
                    </IonCardContent>
                </IonCard>
                <IonCard href="/projects">
                    <IonCardContent className="padding">
                        <div className="projects-card-content">
                            {user.projects ? (
                                <div className="card">
                                    <IonCardTitle>
                                        <h1>Projects </h1>
                                    </IonCardTitle>
                                    <IonCardSubtitle>
                                        <div className="info">
                                            {user.projects.map(p =>
                                                projects
                                                    .filter(ps => ps.id === p)
                                                    .map(e => (
                                                        <div id={e.id}>
                                                            {e.title}
                                                        </div>
                                                    ))
                                            )}
                                        </div>
                                    </IonCardSubtitle>
                                </div>
                            ) : (
                                <div className="card">
                                    <IonCardTitle>
                                        <h1> No Projects </h1>
                                    </IonCardTitle>
                                    <IonCardSubtitle>
                                        <IonButton>Add</IonButton>
                                    </IonCardSubtitle>
                                </div>
                            )}
                        </div>
                    </IonCardContent>
                </IonCard>
            </IonContent>
        </IonPage>
    )
}

export default ProfilePage
