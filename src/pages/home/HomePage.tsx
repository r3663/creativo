import { IonContent, IonPage, IonChip, IonLabel } from '@ionic/react'
import { List, Searchbar, FilterDialog, FilterDisplay } from './Components'
import React, { Dispatch } from 'react'

import { useDispatch } from 'react-redux'
import {
    UserInteractions,
    updateUserSelection,
    updateOptions,
    OptionsAction,
} from '../../store'

import { useFilter, useSearch, useServices } from '../../utils/hooks'
import './styles/layout.css'
import './styles/filter-bar.css'

export const HomePage: React.FC = () => {
    const filter = useFilter()
    const search = useSearch()

    const setRedux = useDispatch<Dispatch<Partial<UserInteractions>>>()
    const setOptions = useDispatch<Dispatch<OptionsAction>>()

    const { q } = search
    const { apply } = filter

    return (
        <IonPage>
            <IonContent fullscreen>
                <Searchbar
                    q={q.toUpperCase()}
                    onChange={q => {
                        setRedux(
                            updateUserSelection({
                                home: {
                                    filter: {
                                        apply: {
                                            ...apply,
                                        },
                                    },
                                    search: {
                                        q,
                                    },
                                },
                            })
                        )
                    }}
                    onClear={() => {
                        setRedux(
                            updateUserSelection({
                                home: {
                                    filter: {
                                        apply: {
                                            ...apply,
                                        },
                                    },
                                    search: {
                                        q: '',
                                    },
                                },
                            })
                        )
                    }}
                ></Searchbar>
                <div className="content">
                    {apply && <FilterDisplay {...apply} />}
                    <List
                        apply={apply}
                        q={q || ''}
                        onChange={q => {
                            setTimeout(() => {
                                setOptions(
                                    updateOptions({
                                        state: {
                                            modal: {
                                                filter: true,
                                            },
                                        },
                                    })
                                )
                            }, 500)
                            setRedux(
                                updateUserSelection({
                                    home: {
                                        filter: {
                                            apply: {
                                                ...apply,
                                            },
                                        },
                                        search: {
                                            q,
                                        },
                                    },
                                })
                            )
                        }}
                    />
                </div>
                <FilterDialog />
            </IonContent>
        </IonPage>
    )
}
