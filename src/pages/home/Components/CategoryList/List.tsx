import { IonList, IonCard, IonCardTitle } from '@ionic/react'

import React from 'react'

import { Category, User, ForFilter } from '../../../../types'
import { useServices } from '../../../../utils/hooks'

import '../../styles/base.css'
import '../../styles/category-list.css'

const CategoryCard = ({
    onChange,
    id,
    name,
    image,
}: {
    onChange: (q: string) => void
} & Partial<Category>) => (
    <IonCard
        className="category-card"
        onClick={() => onChange((name && name.toLowerCase()) || '')}
        key={id}
    >
        <div className="wrapper">
            <img src={'/assets/' + image} alt="" />
            <IonCardTitle className="title" color="light">
                {name}
            </IonCardTitle>
        </div>
    </IonCard>
)

const UserCard = ({
    onChange,
    id,
    name,
    image,
    user,
}: {
    onChange: (q: string) => void
    user: User
} & Partial<Category>) => (
    <IonCard className="user-card" color="secondary" key={user.id}>
        <img src={user.moodboard} alt="" />
        <div className="wrapper">
            <div className="info">
                <h1>{user.username}</h1>
                <h4>{user.title}</h4>
            </div>
            <div className="avatar">
                <img alt={`${user.username}-avatar`} src={user.avatar} />
            </div>
        </div>
    </IonCard>
)

export const List = ({
    q,
    onChange,
    apply,
}: {
    q: string
    onChange: (q: string) => void
    apply: ForFilter
}) => {
    const services = useServices()
    const categories = services.categories.get()

    return (
        <div>
            <div>
                <IonList className="category-list" lines="full">
                    {q === ''
                        ? categories.map(({ id, name, image }) => (
                              <CategoryCard
                                  key={`${id}-${name}-category`}
                                  id={id}
                                  name={name}
                                  image={image}
                                  onChange={onChange}
                              />
                          ))
                        : categories
                              .filter(
                                  c => c.name.toLowerCase() === q.toLowerCase()
                              )
                              .map(({ id, image, name, users }) =>
                                  users.map(u => {
                                      const user = services.users.getOne(u.id)

                                      return (
                                          <UserCard
                                              key={`${id}-${name}-${user.id}-${user.username}`}
                                              id={id}
                                              name={name}
                                              image={image}
                                              onChange={onChange}
                                              user={user}
                                          />
                                      )
                                  })
                              )}
                </IonList>
            </div>
        </div>
    )
}
