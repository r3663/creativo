import { IonInput, IonIcon, IonButtons, IonBackButton } from '@ionic/react'

import React, { Dispatch } from 'react'
import { useDispatch } from 'react-redux'

import { OptionsAction, updateOptions } from '../../../../store'

import { search, settings } from 'ionicons/icons'
import '../../styles/base.css'
import '../../styles/searchbar.css'

const Icons = ({
    q,
    onClear,
}: {
    q: string

    onClear: () => void
}) => (
    <IonButtons className="buttons" slot="start">
        {q !== '' ? (
            <IonBackButton
                onClick={onClear}
                defaultHref="/home"
                text={'search'}
                icon={'arrow-back'}
            />
        ) : (
            <IonIcon className="icon" color="primary" icon={search} />
        )}
    </IonButtons>
)

export const Searchbar = ({
    q,
    onChange,
    onClear,
}: {
    q: string
    onChange: (q: string) => void
    onClear: () => void
}) => {
    const setRedux = useDispatch<Dispatch<OptionsAction>>()
    return (
        <div className="searchbar">
            <Icons q={q} onClear={onClear} />
            <IonInput
                className="input"
                placeholder="search"
                value={q.toUpperCase()}
                onIonChange={e => onChange(e.detail.value || '')}
            ></IonInput>
            <IonButtons slot="end">
                <IonIcon
                    onClick={() =>
                        setRedux(
                            updateOptions({
                                state: {
                                    modal: {
                                        filter: true,
                                    },
                                },
                            })
                        )
                    }
                    className="icon"
                    color="primary"
                    icon={settings}
                />
            </IonButtons>
        </div>
    )
}
