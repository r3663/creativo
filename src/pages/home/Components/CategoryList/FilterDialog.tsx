import {
    IonButton,
    IonContent,
    IonModal,
    IonIcon,
    IonList,
    IonItem,
    IonChip,
    IonLabel,
    IonRange,
    IonDatetime,
    IonSelect,
    IonSelectOption,
} from '@ionic/react'

import React, { Dispatch } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import {
    AppState,
    OptionsAction,
    updateOptions,
    UserInteractions,
    updateUserSelection,
} from '../../../../store'
import { Project } from '../../../../types'
import { useLocationService, useProjectsService } from '../../../../utils/hooks'

import '../../styles/filter-dialog.css'

export const FilterDialog = () => {
    const { get: getLocations, getOne: getLocationById } = useLocationService()
    const { getOne: getProjectById } = useProjectsService()

    const user = useSelector((state: AppState) => state.user)
    const options = useSelector((state: AppState) => state.options)
    const interaction = useSelector((state: AppState) => state.interaction)

    const setInteraction = useDispatch<Dispatch<Partial<UserInteractions>>>()
    const setRedux = useDispatch<Dispatch<OptionsAction>>()

    if (!user.projects) {
        return <div> fallback to create project in list </div>
    }

    if (!options) {
        return <div> no initial state was given </div>
    }

    if (!options.state) {
        return <div> no initial state was given </div>
    }

    if (!options.state.modal) {
        return <div> </div>
    }

    const { filter } = options.state.modal

    const projects: Project[] = []
    for (const project of user.projects) {
        const p = getProjectById(project)
        projects.push(p)
    }

    return (
        <IonContent>
            <IonModal
                cssClass="modal-override"
                backdropDismiss={false}
                isOpen={filter}
            >
                <div className="filter-dialog">
                    <div className="header">
                        <p>filter for</p>
                        <IonButton
                            onClick={() =>
                                setRedux(
                                    updateOptions({
                                        state: {
                                            modal: {
                                                filter: false,
                                            },
                                        },
                                    })
                                )
                            }
                            color="light"
                        >
                            <IonIcon slot="icon-only" name="close" />
                        </IonButton>
                    </div>
                    <div className="projects">
                        <IonList className="list" lines="none">
                            {projects.map(x => (
                                <IonItem key={x.id} className="item">
                                    <IonButton className="list-button">
                                        {x.title}
                                    </IonButton>
                                </IonItem>
                            ))}
                        </IonList>
                    </div>
                    <div className="filter">
                        <IonButton
                            onClick={() => {
                                setRedux(
                                    updateOptions({
                                        state: {
                                            modal: {
                                                filter: false,
                                            },
                                        },
                                    })
                                )

                                setInteraction(
                                    updateUserSelection({
                                        project: {
                                            create: {},
                                        },
                                    })
                                )
                            }}
                            href="/projects/new"
                            className="button"
                        >
                            Add Project
                        </IonButton>
                        <div className="options">
                            <IonItem>
                                <IonLabel>Start</IonLabel>
                                <IonDatetime
                                    value={
                                        interaction.home &&
                                        interaction.home.filter.apply.date &&
                                        interaction.home.filter.apply.date
                                            .start &&
                                        interaction.home.filter.apply.date.start.toString()
                                    }
                                    onIonChange={e => {
                                        if (!interaction.home) {
                                            return
                                        }
                                        setInteraction(
                                            updateUserSelection({
                                                home: {
                                                    ...interaction.home,
                                                    filter: {
                                                        apply: {
                                                            ...interaction.home
                                                                .filter.apply,
                                                            date: {
                                                                ...interaction
                                                                    .home.filter
                                                                    .apply.date,
                                                                start:
                                                                    e.detail
                                                                        .value,
                                                            },
                                                        },
                                                    },
                                                },
                                            })
                                        )
                                    }}
                                ></IonDatetime>
                            </IonItem>
                            <IonItem>
                                <IonLabel>End</IonLabel>
                                <IonDatetime
                                    value={
                                        interaction.home &&
                                        interaction.home.filter.apply.date &&
                                        interaction.home.filter.apply.date
                                            .end &&
                                        interaction.home.filter.apply.date.end.toString()
                                    }
                                    onIonChange={e => {
                                        if (!interaction.home) {
                                            return
                                        }
                                        setInteraction(
                                            updateUserSelection({
                                                home: {
                                                    ...interaction.home,
                                                    filter: {
                                                        apply: {
                                                            ...interaction.home
                                                                .filter.apply,
                                                            date: {
                                                                ...interaction
                                                                    .home.filter
                                                                    .apply.date,
                                                                end:
                                                                    e.detail
                                                                        .value,
                                                            },
                                                        },
                                                    },
                                                },
                                            })
                                        )
                                    }}
                                ></IonDatetime>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Locations</IonLabel>
                                <IonSelect
                                    value={
                                        interaction.home &&
                                        interaction.home.filter.apply.locations
                                    }
                                    multiple={true}
                                    onIonChange={e => {
                                        if (!interaction.home) {
                                            return
                                        }
                                        setInteraction(
                                            updateUserSelection({
                                                home: {
                                                    ...interaction.home,
                                                    filter: {
                                                        apply: {
                                                            ...interaction.home
                                                                .filter.apply,
                                                            locations:
                                                                e.detail.value,
                                                        },
                                                    },
                                                },
                                            })
                                        )
                                    }}
                                >
                                    {getLocations().map((l: any) => {
                                        return (
                                            <IonSelectOption
                                                key={l.id}
                                                value={l.id}
                                            >
                                                {l.city}
                                            </IonSelectOption>
                                        )
                                    })}
                                </IonSelect>
                            </IonItem>
                            <IonItem>
                                <div className="locations">
                                    {interaction.home &&
                                        interaction.home.filter &&
                                        interaction.home.filter.apply &&
                                        interaction.home.filter.apply
                                            .locations &&
                                        interaction.home.filter.apply.locations.map(
                                            l => {
                                                const loc = getLocationById(l)
                                                return (
                                                    <IonChip key={loc.id}>
                                                        <IonLabel>
                                                            {loc.city}
                                                        </IonLabel>
                                                    </IonChip>
                                                )
                                            }
                                        )}
                                </div>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Price</IonLabel>
                                {interaction.home &&
                                    interaction.home.filter &&
                                    interaction.home.filter.apply &&
                                    interaction.home.filter.apply.price && (
                                        <div>
                                            {
                                                interaction.home.filter.apply
                                                    .price.min
                                            }
                                        </div>
                                    )}
                                <IonRange
                                    dualKnobs={true}
                                    min={0}
                                    max={200}
                                    onIonChange={(e: any) => {
                                        if (!interaction.home) {
                                            return
                                        }

                                        setInteraction(
                                            updateUserSelection({
                                                home: {
                                                    ...interaction.home,
                                                    filter: {
                                                        apply: {
                                                            ...interaction.home
                                                                .filter.apply,
                                                            locations:
                                                                interaction.home
                                                                    .filter
                                                                    .apply
                                                                    .locations ||
                                                                [],
                                                            price: {
                                                                max:
                                                                    e.detail
                                                                        .value
                                                                        .upper ||
                                                                    0,
                                                                min:
                                                                    e.detail
                                                                        .value
                                                                        .lower ||
                                                                    0,
                                                            },
                                                        },
                                                    },
                                                },
                                            })
                                        )
                                    }}
                                    step={1}
                                    snaps={false}
                                />

                                {interaction.home &&
                                    interaction.home.filter &&
                                    interaction.home.filter.apply &&
                                    interaction.home.filter.apply.price && (
                                        <div>
                                            {
                                                interaction.home.filter.apply
                                                    .price.max
                                            }
                                        </div>
                                    )}
                            </IonItem>
                        </div>
                        <div className="search">
                            <IonButton
                                onClick={() =>
                                    setRedux(
                                        updateOptions({
                                            state: {
                                                modal: {
                                                    filter: false,
                                                },
                                            },
                                        })
                                    )
                                }
                                className="button"
                            >
                                search
                            </IonButton>
                        </div>
                    </div>
                </div>
            </IonModal>
        </IonContent>
    )
}
