import { IonContent, IonPage, IonChip, IonLabel } from '@ionic/react'
import React from 'react'

import '../styles/layout.css'
import '../styles/filter-bar.css'
import { ForFilter } from '../../../types'

import { useLocationService } from '../../../utils/hooks'

export const FilterDisplay = ({ date, locations, price }: ForFilter) => {
    const { getOne } = useLocationService()

    const startDate = new Date(date.start || new Date())
    const endDate = new Date(date.end || new Date())

    return (
        <div className="filter-bar">
            {startDate && !endDate && (
                <IonChip>
                    <IonLabel>
                        from {startDate.getFullYear()}-{startDate.getMonth()}-
                        {startDate.getMinutes()}
                    </IonLabel>
                </IonChip>
            )}
            {endDate && !startDate && (
                <IonChip>
                    <IonLabel>
                        until {endDate.getFullYear()}-{endDate.getMonth()}-
                        {endDate.getMinutes()}
                    </IonLabel>
                </IonChip>
            )}

            {endDate && startDate && (
                <IonChip>
                    <IonLabel>
                        from {startDate.getFullYear()}-{startDate.getMonth()}-
                        {startDate.getMinutes()} until {endDate.getFullYear()}-
                        {endDate.getMonth()}-{endDate.getMinutes()}
                    </IonLabel>
                </IonChip>
            )}

            <IonChip>
                <IonLabel>
                    from {price.min}$ to {price.max}$
                </IonLabel>
            </IonChip>

            {locations.map(l => {
                const loc = getOne(l)
                return (
                    <IonChip key={loc.id}>
                        <IonLabel>{loc.city}</IonLabel>
                    </IonChip>
                )
            })}
        </div>
    )
}
