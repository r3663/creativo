import React from 'react'
import { IonItem, IonLabel } from '@ionic/react'

import { User, Project, Conversation } from '../../../types'


import '../styles/row.css'

export const ProjectRow = ({
    project,
    users,
}: {
    project: Project
    users: User[]
}) => (
    <IonItem className="project-row-item">
        <div>
            <IonLabel> {project.title} </IonLabel>
            <div>
                {project.users.map(u => {
                    const user = users.filter(x => x.id === u)[0]
                    return <p key={project.id + user.id}>{user.username}</p>
                })}
            </div>
        </div>
    </IonItem>
)

export const ConversationRow = ({
    messages,
    username,
    avatar,
}: Pick<Conversation, 'messages'> & Partial<User>) => (
    <IonItem lines="full" className="conversation-row-item">
        <div>
            <img alt={`${username}-avatar`} src={avatar} />
            <div className="preview">
                <p>{username}</p>
                <div>
                    <p>{messages && messages[0] && messages[0].text}</p>
                </div>
            </div>
        </div>
        <div></div>
    </IonItem>
)
