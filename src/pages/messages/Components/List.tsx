import React, { Dispatch } from 'react'
import {
    IonLabel,
    IonList,
    IonListHeader,
    IonSegment,
    IonSegmentButton,
} from '@ionic/react'

import { MESSAGES_PAGE, UserSelection } from '../../../types'

import { ConversationRow } from './Row'

import { AppState, UserInteractions, updateUserSelection } from '../../../store'

import '../styles/message-list.css'
import { useSelector, useDispatch } from 'react-redux'

export const MessageList = () => {
    const setRedux = useDispatch<Dispatch<Partial<UserInteractions>>>()
    const user = useSelector((state: AppState) => state.user)
    const { app } = useSelector((state: AppState) => state.services)
    const { messages } = useSelector((state: AppState) => state.interaction)

    const changeTab = (selection: Partial<UserSelection>) =>
        setRedux(updateUserSelection(selection))

    if (!app) {
        return <div> No services </div>
    }

    if (!messages) {
        return <div> no messages </div>
    }

    if (!messages.view) {
        return <div> no view selected </div>
    }

    if (!user.projects) {
        return <div> start a new project </div>
    }

    if (!user.conversations) {
        return <div> start chatting with someone </div>
    }

    const { projects, conversations, users } = app
    const { view } = messages

    return (
        <div className="message-list">
            <div className="tabs">
                <IonSegment
                    value={view}
                    onIonChange={e =>
                        changeTab({
                            messages: {
                                view:
                                    (e.detail.value as any) ||
                                    MESSAGES_PAGE.PERSONAL,
                            },
                        })
                    }
                >
                    <IonSegmentButton value={MESSAGES_PAGE.PERSONAL}>
                        <IonLabel>Conversations</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton value={MESSAGES_PAGE.PROJECT}>
                        <IonLabel>Projects</IonLabel>
                    </IonSegmentButton>
                </IonSegment>
            </div>

            <IonList lines="full">
                {view === MESSAGES_PAGE.PROJECT && (
                    <>
                        <IonListHeader>Projects</IonListHeader>
                        {user.projects.map(m => {
                            const project = projects.filter(x => x.id === m)[0]
                            return (
                                <p key={project.id}>
                                    {project.conversation.id}
                                </p>
                            )
                            // return <ProjectRow users={[]} project={project} />
                        })}
                    </>
                )}
                {view === MESSAGES_PAGE.PERSONAL && (
                    <>
                        <IonListHeader>Conversations</IonListHeader>
                        {user.conversations.map(m => {
                            const conversation = conversations.filter(
                                x => x.id === m
                            )[0]

                            if (!conversation) {
                                return []
                            }

                            const npc = users.filter(
                                x => x.id === conversation.users[0]
                            )[0]

                            return (
                                <ConversationRow
                                    key={conversation.id}
                                    messages={conversation.messages}
                                    avatar={npc.avatar}
                                    username={npc.username}
                                />
                            )
                        })}
                    </>
                )}
            </IonList>
        </div>
    )
}
