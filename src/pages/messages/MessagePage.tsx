import React from 'react'
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
} from '@ionic/react'

import { MessageList } from './Components'

export const MessagePage: React.FC = () => (
    <IonPage>
        <IonHeader>
            <IonToolbar>
                <IonTitle>Messages</IonTitle>
            </IonToolbar>
        </IonHeader>
        <IonContent>
            <MessageList />
        </IonContent>
    </IonPage>
)

export default MessagePage
