import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { createStore } from 'redux'
import { rootReducer, AppState } from './store'

import { Provider } from 'react-redux'
import { bootstrap } from './utils'
import { composeWithDevTools } from 'redux-devtools-extension'
import { persistStore, persistReducer } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import storage from 'redux-persist/lib/storage'
import { StateRunner } from './utils/StateRunner'
import { envs } from './utils/StateHydrator'

// read the app configuration and form the params
const params = window.location.search.substring(1)
const optionsMap = params.split('&').map(pair => {
    const assign = pair.split('=')
    const key = assign[0]
    const value = assign[1]
    return {
        key,
        value,
    }
})
// initialise the mocked data
const { hydrator, withParams } = bootstrap(
    (optionsMap.filter(v => v.key === 'mock')[0] &&
        (optionsMap.filter(v => v.key === 'mock')[0].value as envs)) ||
        'production',
    optionsMap
)
// setup the reducers and persist the  reducer
const bootstrapApp = (state: AppState) => {
    const persistConfig = {
        key: 'root',
        storage,
    }
    const persistedReducer = persistReducer(persistConfig, rootReducer)

    const store = createStore(
        persistedReducer,
        {
            options: state.options,
            interaction: state.interaction,
            user: state.user,
            services: state.services,
        },
        composeWithDevTools()
    )
    const persistor = persistStore(store)
    persistor.purge()

    return { persistor, store }
}

// call
const { store, persistor } = bootstrapApp(withParams)

const stateRunner = new StateRunner(hydrator, persistor)

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            {withParams.options.debug ? stateRunner.walk() : <App />}
        </PersistGate>
    </Provider>,
    document.getElementById('root')
)
