import React, { Dispatch } from 'react'
import { Redirect, Route } from 'react-router-dom'
import {
    IonApp,
    IonIcon,
    IonRouterOutlet,
    IonTabBar,
    IonTabButton,
    IonTabs,
    IonSpinner,
} from '@ionic/react'
import { IonReactRouter } from '@ionic/react-router'

import { routes } from './types'

import { UserInteractions, navigate, AppState } from './store'
import { useLoading } from './utils'

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css'

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css'
import '@ionic/react/css/structure.css'
import '@ionic/react/css/typography.css'

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css'
import '@ionic/react/css/float-elements.css'
import '@ionic/react/css/text-alignment.css'
import '@ionic/react/css/text-transformation.css'
import '@ionic/react/css/flex-utils.css'
import '@ionic/react/css/display.css'

/* Theme variables */
import './theme/variables.css'
import { useDispatch, useSelector } from 'react-redux'

const App: React.FC = () => {
    const state = useSelector((state: AppState) => state.options.state)
    const setRedux = useDispatch<Dispatch<UserInteractions>>()
    const route = useSelector((state: AppState) => state.interaction.route)
    useLoading(false)

    if (!route) {
        return <div> no route </div>
    }

    if (!state) {
        return <div> no state </div>
    }
    const { tab } = route

    try {
        return (
            <IonApp>
                {state.isLoading && (
                    <div
                        style={{
                            position: 'absolute',
                            zIndex: 1000,
                            width: '100%',
                            height: '100%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            display: 'flex',
                            backgroundColor: 'rgba(0, 0, 0, 0.5)',
                        }}
                    >
                        <IonSpinner
                            name="crescent"
                            style={{ width: '100%', height: '20%' }}
                        />
                    </div>
                )}
                <IonReactRouter>
                    <IonTabs>
                        <IonRouterOutlet>
                            {routes.map(({ path, exact, component, label }) => {
                                return (
                                    <Route
                                        key={path + label}
                                        path={path}
                                        component={component}
                                        exact={exact}
                                    />
                                )
                            })}
                            <Route
                                exact
                                path="/"
                                render={() => <Redirect to={'/home'} />}
                            />
                        </IonRouterOutlet>
                        <IonTabBar
                            style={{
                                backgroundColor: 'white',
                            }}
                            slot="bottom"
                            selectedTab={tab}
                        >
                            {routes.map(route => (
                                <IonTabButton
                                    onClick={() => {
                                        setRedux(navigate(route))
                                    }}
                                    key={route.path + route.tab}
                                    tab={route.tab}
                                    href={route.path as any}
                                >
                                    <IonIcon icon={route.icon} />
                                </IonTabButton>
                            ))}
                        </IonTabBar>
                    </IonTabs>
                </IonReactRouter>
            </IonApp>
        )
    } catch (e) {
        return <div> Error </div>
    }
}

export default App
