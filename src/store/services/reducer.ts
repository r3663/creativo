import { Services } from '../../types'

import { ServiceActions } from './actions'

export function services(
    state: Partial<Services> = {},
    action: ServiceActions
): Partial<Services> {
    switch (action.type) {
        default:
            return state
    }
}
