import { Services } from '../../types'
export const UPDATE_SERVICES = 'update/options/action'

interface ServiceUpdateAction {
    type: typeof UPDATE_SERVICES
    services: Services
}

export type ServiceActions = ServiceUpdateAction
