import { Options } from '../../types'

import { Action, ActionCreator } from 'redux'

export const UPDATE_OPTION = 'update/options/action'

interface UpdateOptionsAction extends Action {
    type: typeof UPDATE_OPTION
    options: Options
}

export const updateOptions: ActionCreator<UpdateOptionsAction> = (
    options: Options
) => ({
    type: UPDATE_OPTION,
    options,
})

export type OptionsAction = UpdateOptionsAction
