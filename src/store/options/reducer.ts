import { OptionsAction, UPDATE_OPTION } from './actions'
import { Options } from '../../types'

export function options(
    state: Partial<Options> = {},
    action: OptionsAction
): Partial<Options> {
    switch (action.type) {
        case UPDATE_OPTION:
            return { ...state, ...action.options }
        default:
            return state
    }
}
