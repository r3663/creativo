import { UserSelection } from '../../types'
import {
    UserInteractions,
    UPDATE_SELECTION,
    UPDATE_ROUTE_NAVIGATE,
} from './actions'

export function interaction(
    state: Partial<UserSelection> = {},
    action: UserInteractions
): Partial<UserSelection> {
    switch (action.type) {
        case UPDATE_ROUTE_NAVIGATE:
            return {
                ...state,
                ...action.route,
            }
        case UPDATE_SELECTION:
            return {
                ...state,
                ...action.useractions,
            }
        default:
            return state
    }
}
