import { RouteP, UserSelection } from '../../types'

import { Action, ActionCreator } from 'redux'

export const UPDATE_SELECTION = 'update/selection/action'
export const UPDATE_ROUTE_NAVIGATE = 'update/selection/route/action'

// navigate to anoter page in the view
// this is not implemented atm and part
// of a future release
interface NavigateAction extends Action {
    type: typeof UPDATE_ROUTE_NAVIGATE
    route: RouteP
}

export const navigate: ActionCreator<NavigateAction> = (route: RouteP) => ({
    type: UPDATE_ROUTE_NAVIGATE,
    route,
})

// update what the user did, his interaction
interface UpdateUserSelections extends Action {
    type: typeof UPDATE_SELECTION
    useractions: UserSelection
}

export const updateUserSelection: ActionCreator<UpdateUserSelections> = (
    useractions: UserSelection
) => ({
    type: UPDATE_SELECTION,
    useractions,
})

export type UserInteractions = UpdateUserSelections | NavigateAction
