import { combineReducers } from 'redux'
import { interaction } from './interactions'
import { user } from './user'
import { services } from './services'
import { options } from './options'

export const rootReducer = combineReducers({
    interaction,
    user,
    options,
    services,
})

export type AppState = ReturnType<typeof rootReducer>

export * from './user'
export * from './interactions'
export * from './options'
export * from './services'
