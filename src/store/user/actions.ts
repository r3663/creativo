import { User } from '../../types'

import { Action, ActionCreator } from 'redux'

export const UPDATE_USER = 'update/user/action'
// update the user himself
interface UpdateUserAction extends Action {
    type: typeof UPDATE_USER
    user: User
}

export const updateUser: ActionCreator<UpdateUserAction> = (user: User) => ({
    type: UPDATE_USER,
    user,
})

export type UserActions = UpdateUserAction
