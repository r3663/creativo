import { UserActions, UPDATE_USER } from './actions'
import { User } from '../../types'

export function user(
    state: Partial<User> = {},
    action: UserActions
): Partial<User> {
    switch (action.type) {
        case UPDATE_USER:
            return { ...state, ...action.user }
        default:
            return state
    }
}
