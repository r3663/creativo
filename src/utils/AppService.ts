import {
    User,
    Category,
    Project,
    Message,
    Conversation,
    ProjectLocation,
    Params,
} from '../types'

import { StateGenerator } from './StateGenerator'
import { StateHydrator, envs } from './StateHydrator'

export const bootstrap = (env: envs, optionsMap: Params[]) => {
    const hydrator = new StateHydrator()
    const state = hydrator.getState(env, optionsMap)

    const withParams = { ...state, state: { params: optionsMap } }
    if (withParams.options.debug) {
        console.log(`${env}`, withParams, optionsMap)
    }
    return { hydrator, withParams }
}

interface AppServiceInterface {}

export class AppServiceClass implements AppServiceInterface {
    private user: User
    private users: User[]
    private projects: Project[]
    private categories: Category[]
    private conversations: Conversation[]
    private messages: Message[]
    private locations: ProjectLocation[]

    constructor(
        private generator = new StateGenerator(),
        private hydrator = new StateHydrator()
    ) {
        const {
            services,
            interaction: { profile },
        } = this.hydrator.getState('mock') // this is crap
        if (!services) {
            throw new Error('Services not available')
        }

        if (!services.app) {
            throw new Error('App Services not available')
        }

        if (!profile) {
            throw new Error('No user present!')
        }
        // this state should be acceptable
        // as it means you are just logged out
        if (!profile.user) {
            throw new Error('No user present!')
        }

        this.users = services.app.users
        this.user = profile.user
        this.conversations = services.app.conversations
        this.locations = services.app.locations
        this.categories = services.app.categories
        this.projects = services.app.projects
        this.messages = services.app.messages
    }

    public getLoginedUser() {
        return this.user
    }

    public getCategories() {
        return this.categories
    }

    public getUser(id: string) {
        return this.users.filter(user => user.id === id)[0]
    }

    public getConversations() {
        return this.conversations
    }

    public getMessages() {
        return this.messages
    }

    public getProjects() {
        return this.projects
    }

    public getLocations() {
        return this.locations
    }

    public getUsers() {
        return this.users
    }

    public getCategory(id: string) {
        return this.categories.filter(cat => cat.id === id)[0]
    }

    public login(user: User, password: string) {
        return true
    }
}
