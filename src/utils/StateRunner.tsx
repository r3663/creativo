import React from 'react'
import { routes } from '../types'
import { StateHydrator } from './StateHydrator'
import {
    IonApp,
    IonTabs,
    IonRouterOutlet,
    IonTabBar,
    IonTabButton,
    IonIcon,
    IonLabel,
} from '@ionic/react'
import { IonReactRouter } from '@ionic/react-router'
import { Route, Redirect } from 'react-router'
import { Persistor } from 'redux-persist'

export class StateRunner {
    constructor(
        private hydrator: StateHydrator,
        private persisitor: Persistor
    ) {}

    public walk() {
        return (
            <IonApp>
                <IonReactRouter>
                    <IonTabs>
                        <IonRouterOutlet>
                            {routes.map(({ path, exact, component, label }) => {
                                return (
                                    <Route
                                        key={path + label}
                                        path={path}
                                        component={component}
                                        exact={exact}
                                    />
                                )
                            })}
                            <Route
                                exact
                                path="/"
                                render={() => <Redirect to={`/home`} />}
                            />
                        </IonRouterOutlet>
                        <IonTabBar color="primary" slot="bottom">
                            {routes.map(({ path, icon, tab, label }) => (
                                <IonTabButton
                                    key={path + tab}
                                    tab={tab}
                                    href={path as any}
                                >
                                    <IonIcon icon={icon} />
                                    <IonLabel>{label}</IonLabel>
                                </IonTabButton>
                            ))}
                        </IonTabBar>
                    </IonTabs>
                </IonReactRouter>
            </IonApp>
        )
    }
}
