import faker from 'faker'
import {
    User,
    Category,
    Conversation,
    Message,
    ProjectDate,
    ProjectLocation,
} from '../types'

export type LoadedCategories =
    | 'foto'
    | 'artists'
    | 'model'
    | 'makeup'
    | 'music'
    | 'hairstyle'
    | 'video'
    | 'mask'
    | 'regis'
export class StateGenerator {
    project = (
        owner: string,
        users: string[],
        conversation: Conversation,
        date: ProjectDate,
        description: string,
        location: ProjectLocation
    ) => {
        const id = faker.random.uuid()
        const title = faker.lorem.words(1)

        return {
            id,
            title,
            owner,
            date,
            users,
            conversation,
            description,
            location,
        }
    }
    conversation = (users: string[], owner: string, messages: Message[]) => {
        const id = faker.random.uuid()

        return {
            id,
            owner,
            users,
            messages,
        }
    }
    message = (sender: string) => {
        const id = faker.random.uuid()
        return {
            id,
            sender,
            text: faker.random.words(),
            date: faker.date.past(),
        }
    }
    location = () => {
        const id = faker.random.uuid()
        return {
            id,
            zipCode: faker.address.zipCode(),
            city: faker.address.city(),
            streetName: faker.address.streetName(),
            streetAddress: faker.address.streetAddress(),
            country: faker.address.county(),
            countryCode: faker.address.countryCode(),
            state: faker.address.state(),
        }
    }

    user = () => {
        const id = faker.random.uuid()
        return {
            id,
            conversations: [] as string[],
            projects: [] as string[],
            username: faker.internet.userName(),
            title: faker.name.title(),
            moodboard: faker.random.image(),
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            email: faker.internet.email(),
            avatar: faker.internet.avatar(),
            location: this.location(),
        }
    }

    category = (category: LoadedCategories, users: User[]): Category => {
        return {
            id: faker.random.uuid(),
            users,
            image: this.categoryImage(category),
            name: this.categoryName(category),
        }
    }

    private categoryName = (category: LoadedCategories) => {
        switch (category) {
            case 'foto':
                return 'Fotografen'
            case 'model':
                return 'Model'
            case 'artists':
                return 'artists'
            case 'makeup':
                return 'Makeup Artist'
            case 'hairstyle':
                return 'Hairstylist'
            case 'video':
                return 'Videograf'
            case 'music':
                return 'Musiker'
            case 'mask':
                return 'Masken Bildner'
            case 'regis':
            default:
                return 'Regisseur'
        }
    }

    private categoryImage = (category: LoadedCategories) => {
        switch (category) {
            case 'regis':
                return 'regisseur.jpeg'
            case 'video':
                return 'videograf.jpeg'
            case 'model':
                return 'model.jpeg'
            case 'artists':
                return 'artist.jpeg'
            case 'foto':
                return 'fotograf.jpeg'
            case 'makeup':
                return 'makeup.jpeg'
            case 'mask':
                return 'mask.jpeg'
            case 'music':
                return 'musician.jpeg'
            case 'hairstyle':
                return 'musician.jpeg'
        }
    }
}
