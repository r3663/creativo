import React, { useEffect, useState, Dispatch, useMemo } from 'react'
import { useDispatch, useSelector, shallowEqual } from 'react-redux'
import { UserSelection, RouteP, ForFilter } from '../types'

import {
    AppState,
    UserInteractions,
    updateUserSelection,
    navigate,
    OptionsAction,
    updateOptions,
} from '../store'

import { Redirect } from 'react-router-dom'

export const useLocationService = () => {
    const { app } = useSelector((state: AppState) => state.services)
    if (!app) {
        throw new Error('App Services not loaded')
    }

    if (!app.locations) {
        throw new Error('App Service -- Location not loaded')
    }
    const { locations } = app
    const get = () => locations
    const getOne = (id: string) => locations.filter(x => x.id === id)[0]
    return { get, getOne }
}

export const useUserService = () => {
    const { app } = useSelector((state: AppState) => state.services)
    if (!app) {
        throw new Error('App Services not loaded')
    }

    if (!app.users) {
        throw new Error('App Service -- Location not loaded')
    }
    const { users } = app
    const get = () => users
    const getOne = (id: string) => users.filter(x => x.id === id)[0]
    const getFiltered = (apply: ForFilter) => {
        let filtered = []
        if (apply.locations) {
            for (const loc of apply.locations) {
                filtered.push(getOne(loc))
            }
        }
        return users
    }

    return { get, getOne, getFiltered }
}

export const useProjectsService = () => {
    const { app } = useSelector((state: AppState) => state.services)
    if (!app) {
        throw new Error('App Services not loaded')
    }

    if (!app.projects) {
        throw new Error('App Service -- Projects not loaded')
    }
    const { projects } = app
    const get = () => projects
    const getOne = (id: string) => projects.filter(x => x.id === id)[0]
    return { get, getOne }
}

export const useCategoryService = () => {
    const { app } = useSelector((state: AppState) => state.services)
    if (!app) {
        throw new Error('App Services not loaded')
    }

    if (!app.categories) {
        throw new Error('App Service -- Projects not loaded')
    }
    const { categories } = app
    const get = () => categories
    const getOne = (id: string) => categories.filter(x => x.id === id)[0]
    return { get, getOne }
}

export const useServices = () => {
    return {
        locations: useLocationService(),
        users: useUserService(),
        projects: useProjectsService(),
        categories: useCategoryService(),
    }
}

export const useFilter = () => {
    const { home } = useSelector((state: AppState) => state.interaction)
    if (!home) {
        throw new Error('State not initalized -- home page')
    }

    if (!home.filter) {
        throw new Error('State not initalized -- home page -- filter')
    }

    return home.filter
}

export const useSearch = () => {
    const { home } = useSelector((state: AppState) => state.interaction)
    if (!home) {
        throw new Error('State not initalized -- home page')
    }

    if (!home.search) {
        throw new Error('State not initalized -- home page -- filter')
    }

    return home.search
}

export const useProjectPageSelections = () => {
    const { create } = useSelector((state: UserSelection) => state.projects)
    return { create }
}
export const useHomePageSelections = () => {
    const { filter, search } = useSelector((state: UserSelection) => state.home)
    return { filter, search }
}

// this is not perfect, but it works
// there should be some improvement on the useEffect
// https://reactjs.org/docs/hooks-reference.html#useeffect
// https://reactjs.org/docs/hooks-faq.html#what-can-i-do-if-my-effect-dependencies-change-too-often
// https://reactjs.org/docs/hooks-faq.html#is-it-safe-to-omit-functions-from-the-list-of-dependencies
export function useUserSelections() {
    const useractionSelector = useMemo(
        () => (state: AppState) => state.interaction,
        []
    )
    const setRedux = useDispatch<Dispatch<UserInteractions>>()
    const { debug } = useSelector(
        (state: AppState) => state.options,
        shallowEqual
    )
    const { home, profile, projects, route } = useSelector(useractionSelector)

    const state = {
        home,
        profile,
        projects,
        route,
    }

    const [useractions, setState] = useState<Partial<UserSelection>>(state)
    const [hydrating, setHydrating] = useState<boolean>(true)
    // const updateSelections = useCallback(
    // () => setRedux(updateUserSelection(state as any)),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    // [useractions]
    // )
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => {
        if (debug) {
            console.log(1.1, 'hydrating', hydrating)
        }

        if (hydrating) {
            if (!home) {
                throw new Error('home not set')
            }
            if (!projects) {
                throw new Error('projects not set')
            }
            if (!profile) {
                throw new Error('profile not set')
            }
            const state = {
                home,
                route,
                projects,
                profile,
            }

            setState(() => ({ ...state }))

            setHydrating(false)
            if (debug) {
                console.log(1, 'local', '=========')
                console.log(1.1, 'setting state', { ...state })
                console.info(1.2, 'hydrating done', hydrating)
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [debug, hydrating, home, projects, profile, route])

    const setUseractions: Dispatch<
        React.SetStateAction<Partial<UserSelection> | undefined>
    > = (prev: React.SetStateAction<Partial<UserSelection> | undefined>) => {
        setState(old => {
            const state = {
                ...old,
                ...prev,
            }
            if (debug) {
                console.log(2, 'local', '=========')
                console.log(2.1, 'local', 'old', old)
                console.log(2.2, 'local', 'prev', prev)
                console.log(2.3, 'local', 'new', state)
            }
            setRedux(updateUserSelection(state as any))

            return { ...state }
        })
    }

    return { useractions, setUseractions, setHydrating }
}

export function useNavigation(route: RouteP) {
    const setRedux = useDispatch<Dispatch<UserInteractions>>()
    const current = useSelector((state: AppState) => state.interaction.route)
    const redirect = () => <Redirect to={`${route.path}`} />

    useEffect(() => {
        setRedux(navigate(route))

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [current])

    return { redirect }
}

export function useLoading(isLoading: boolean) {
    const { debug } = useSelector(
        (state: AppState) => state.options,
        shallowEqual
    )
    const setOptions = useDispatch<Dispatch<OptionsAction>>()

    useEffect(() => {
        if (debug) {
            console.log('[DEBUG]', 'is loading', '=========', isLoading)
        }
        setTimeout(() => {
            setOptions(
                updateOptions({
                    state: {
                        isLoading,
                    },
                })
            )
        }, 2000)
    }, [debug, isLoading, setOptions])
}
