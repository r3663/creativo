import { StateGenerator, LoadedCategories } from './StateGenerator'
import { CREATE_VIEW, MESSAGES_PAGE } from '../types'

import { AppState } from '../store'

import {
    User,
    Category,
    Project,
    Message,
    Conversation,
    ProjectLocation,
    Params,
} from '../types'
import { home } from 'ionicons/icons'

export type envs =
    | 'production'
    | 'development'
    | 'mock'
    | 'mock-q-model'
    | 'mock-q-model-filter'
    | 'mock-q-no-result'
    | 'mock-initial-state'
    | 'mock-create-view-fullfilled'

export class StateHydrator extends StateGenerator {
    private generateRandomEnvironment(
        amount = 4
    ): {
        user: User
        users: User[]
        projects: Project[]
        categories: Category[]
        conversations: Conversation[]
        messages: Message[]
        locations: ProjectLocation[]
    } {
        const user = this.user()
        const users = []
        const locations = []
        const conversations = []
        const categories = []
        const projects = []
        const messages = []

        // base
        for (let i = 0; i <= 5; i++) {
            const base = this.user()
            locations.push(base.location)
            conversations.push(this.conversation([base.id], user.id, []))
            users.push(base)
        }

        // categories
        for (let i = 0; i <= 5; i++) {
            switch (i) {
                case 0:
                    const models = this.generateRandomCategory('model')
                    categories.push(...models.categories)
                    users.push(...models.npcs)
                    locations.push(...models.locations)
                    break
                case 1:
                    const artists = this.generateRandomCategory('artists')
                    categories.push(...artists.categories)
                    users.push(...artists.npcs)
                    locations.push(...artists.locations)
                    break
                case 2:
                    const foto = this.generateRandomCategory('foto')
                    categories.push(...foto.categories)
                    users.push(...foto.npcs)
                    locations.push(...foto.locations)
                    break
                case 3:
                    const regis = this.generateRandomCategory('regis')
                    categories.push(...regis.categories)
                    users.push(...regis.npcs)
                    locations.push(...regis.locations)
                    break
            }
        }

        // small talk
        for (let i = 0; i <= 5; i++) {
            const sm = this.user()
            const m = []
            for (let y = 0; y <= 7; y++) {
                m.push(this.message(i % 2 === 0 ? sm.id : user.id))
            }

            locations.push(sm.location)
            messages.push(...m)
            const conversation = this.conversation([sm.id], user.id, m)
            m.map(i => user.conversations.push(i.id))
            conversations.push(conversation)
        }

        // projects
        const userProjects = this.generateRandomProject(user)
        users.push(...userProjects.npcs)
        projects.push(...userProjects.projects)
        conversations.push(...userProjects.conversations)
        locations.push(...userProjects.locations)
        messages.push(...userProjects.messages)

        userProjects.conversations.map(i => user.conversations.push(i.id))
        userProjects.projects.map(i => user.projects.push(i.id))

        return {
            user,
            users,
            locations,
            conversations,
            categories,
            messages,
            projects,
        }
    }

    private generateRandomProject(
        user: User
    ): {
        npcs: User[]
        projects: Project[]
        conversations: Conversation[]
        messages: Message[]
        locations: ProjectLocation[]
    } {
        const locations = []
        const npcs = []
        const conversations = []
        const messages = []
        const projects = []

        for (let i = 0; i <= 5; i++) {
            const npc = this.user()
            locations.push(npc.location)
            npcs.push(npc)
            const projectMessages = []
            for (let y = 0; y <= 7; y++) {
                projectMessages.push(
                    this.message(i % 2 === 0 ? npc.id : user.id)
                )
            }

            const conversation = this.conversation(
                [npc.id],
                user.id,
                projectMessages
            )

            const p = this.project(
                user.id,
                [npc.id],
                conversation,
                {
                    start: new Date(),
                    end: new Date(),
                },
                'A small project with some people',
                npc.location
            )

            conversations.push(conversation)
            messages.push(...projectMessages)
            projects.push(p)
        }

        return { npcs, locations, conversations, messages, projects }
    }

    private generateRandomCategory(
        name: LoadedCategories
    ): {
        npcs: User[]
        categories: Category[]
        locations: ProjectLocation[]
    } {
        const locations = []
        const npcs = []
        const categories = []

        for (let y = 0; y <= 5; y++) {
            const npc = this.user()
            locations.push(npc.location)
            npcs.push(npc)
        }

        categories.push(this.category(name, npcs))

        return { npcs, categories, locations }
    }

    public getState(env: envs, params: Params[] = []): AppState {
        const {
            locations,
            users,
            categories,
            projects,
            conversations,
            messages,
            user,
        } = this.generateRandomEnvironment()

        const debug: any =
            (params.filter(o => o.key === 'debug')[0] &&
                params.filter(o => o.key === 'debug')[0].value &&
                true) ||
            false

        const services = {
            app: {
                users,
                projects,
                conversations,
                categories,
                locations,
                messages,
            },
        }
        const interaction = {
            route: {
                tab: 'projects' as any,
                exact: true,
                path: '/projects',
                label: 'projects' as any,
                icon: home,
            },
            // this must be removed!
            profile: {
                user,
            },
            home: {
                filter: {
                    apply: {
                        locations: [] as string[],
                        date: {},
                        price: {
                            min: 0,
                            max: 200,
                        },
                    },
                    date: {},
                    description: '',
                    locations: locations.map(l => l.id),
                    title: 'the first projekt i ever made',
                    users: users.map(u => u.id),
                },
                search: {
                    q: '',
                },
            },
            messages: {
                view: MESSAGES_PAGE.PERSONAL,
            },
            projects: {
                create: {
                    view: CREATE_VIEW.CREATE,
                    date: {},
                    description: '',
                    locations: locations.map(l => l.id),
                    title: '',
                    users: users.map(u => u.id),
                },
            },
        }

        const evalEnv = (env: envs): AppState => {
            switch (env) {
                case 'mock-q-model-filter':
                    return {
                        user,
                        services,
                        interaction: {
                            ...interaction,
                            home: {
                                filter: {
                                    apply: {
                                        date: {
                                            start: '2017-12-02T00:00:00+01:00' as any,
                                            end: '2017-12-02T00:00:00+01:00' as any,
                                        },
                                        locations: [
                                            locations[0].id,
                                            locations[1].id,
                                        ] as string[],
                                        price: {
                                            min: 0,
                                            max: 200,
                                        },
                                    },
                                },
                                search: {
                                    q: 'model',
                                },
                            },
                        },
                        options: {
                            state: {
                                isLoggedIn: false,
                                hasError: false,
                                isLoading: true,
                                modal: {
                                    filter: true,
                                },
                            },
                            debug,
                            params,
                            env,
                        },
                    }
                case 'mock':
                    return {
                        user,
                        services,
                        interaction,
                        options: {
                            state: {
                                isLoggedIn: false,
                                hasError: false,
                                isLoading: true,
                                modal: {
                                    filter: false,
                                },
                            },
                            debug,
                            params,
                            env,
                        },
                    }
                case 'mock-q-no-result':
                    return {
                        user,
                        services,
                        interaction: {
                            ...interaction,
                            home: {
                                filter: {
                                    apply: {
                                        date: {},
                                        locations: [] as string[],
                                        price: {
                                            min: 0,
                                            max: 200,
                                        },
                                    },
                                },
                                search: {
                                    q: 'NO RESULT FOUND',
                                },
                            },
                        },
                        options: {
                            state: {
                                isLoggedIn: false,
                                hasError: false,
                                isLoading: true,
                                modal: {
                                    filter: false,
                                },
                            },
                            debug,
                            params,
                            env,
                        },
                    }
                case 'mock-q-model':
                    return {
                        user,
                        services,
                        interaction: {
                            ...interaction,
                            home: {
                                filter: {
                                    apply: {
                                        date: {},
                                        locations: [] as string[],
                                        price: {
                                            min: 0,
                                            max: 200,
                                        },
                                    },
                                },
                                search: {
                                    q: 'MODEL',
                                },
                            },
                        },
                        options: {
                            state: {
                                isLoggedIn: false,
                                hasError: false,
                                isLoading: true,
                                modal: {
                                    filter: false,
                                },
                            },
                            debug,
                            params,
                            env,
                        },
                    }
                case 'mock-create-view-fullfilled':
                    return {
                        user,
                        services,
                        interaction: {
                            ...interaction,
                            projects: {
                                create: {
                                    view: CREATE_VIEW.CREATE,
                                    date: {},
                                    description: 'This is my beautiful wedding',
                                    locations: locations.map(l => l.id),
                                    title: 'Wedding',
                                    users: users.map(u => u.id),
                                },
                            },
                        },
                        options: {
                            state: {
                                isLoggedIn: false,
                                hasError: false,
                                isLoading: true,
                                modal: {
                                    filter: false,
                                },
                            },
                            debug,
                            params,
                            env,
                        },
                    }
                default:
                    return {
                        user,
                        services,
                        interaction,
                        options: {
                            state: {
                                isLoggedIn: false,
                                hasError: false,
                                isLoading: true,
                                modal: {
                                    filter: false,
                                },
                            },
                            debug,
                            params,
                            env,
                        },
                    }
            }
        }

        return evalEnv(env)
    }
}
