import {
    search,
    starOutline,
    checkboxOutline,
    radioButtonOff,
} from 'ionicons/icons'
import { RouteProps } from 'react-router-dom'

import { HomePage } from './pages/home'
import { MessagePage } from './pages/messages'
import { ProjectPage } from './pages/projects'
import { ProfilePage } from './pages/profile'

export interface Params {
    key: string
    value: string
}

interface ApplicationState {
    hasError: boolean
    isLoading: boolean
    isLoggedIn: boolean
    modal: {
        filter: boolean
    }
}

export interface Options {
    state: ApplicationState
    debug: boolean
    params: Params[]
    env: string
}

export interface Conversation {
    id: string
    users: string[]
    owner: string
    messages: Message[]
}

export interface Message {
    id: string
    date: Date
    text: string
    sender: string
}

export interface ProjectDate {
    id?: string
    start?: Date
    end?: Date
}

export interface ProjectLocation {
    id: string
    zipCode: string
    city: string
    streetName: string
    streetAddress: string
    country: string
    countryCode: string
    state: string
}

export interface Project {
    id: string
    owner: string
    users: string[]
    title: string
    location: ProjectLocation
    date: ProjectDate
    description: string
    moodboard?: string
    conversation: Conversation
}

export interface Portfolio {}

export interface User {
    id: string
    email: string
    avatar: string
    firstName: string
    lastName: string
    username: string
    projects: string[]
    moodboard: string
    // portofolio
    conversations: string[]
    title: string
    // there should be a location
    // if not the user will be asked
    // to give us his location
    location?: ProjectLocation
}

export interface Category {
    id: string
    name: string
    image: string
    users: User[]
}
export type ModuleNames = 'projects' | 'messages' | 'profile' | 'home'
export type RouteP = RouteProps & {
    tab: ModuleNames
    label: ModuleNames
    icon: any
}
export interface ForFilter {
    locations: Array<string>
    date: {
        start?: Date
        end?: Date
    }
    price: {
        min: number
        max: number
    }
}

export interface UserSelection {
    route: Omit<RouteP, 'component'>
    profile: {
        user?: User
    }
    home: {
        search?: {
            q: string
        }
        filter: {
            apply: ForFilter
        }
    }
    messages: MessageProps
    projects: {
        create?: CreateProjectProps
    }
}

export enum CREATE_VIEW {
    CREATE = 'create',
    REVIEW = 'review',
}

export enum MESSAGES_PAGE {
    PERSONAL = 'personal',
    PROJECT = 'project',
}

export const routes: Array<RouteP> = [
    {
        tab: 'home',
        component: HomePage,
        exact: true,
        path: '/home',
        label: 'home',
        icon: search,
    },
    {
        tab: 'messages',
        component: MessagePage,
        exact: true,
        path: '/messages',
        label: 'messages',
        icon: starOutline,
    },
    {
        tab: 'projects',
        component: ProjectPage,
        exact: true,
        path: '/projects/new',
        label: 'projects',
        icon: checkboxOutline,
    },
    {
        tab: 'projects',
        component: ProjectPage,
        exact: true,
        path: '/projects',
        label: 'projects',
        icon: checkboxOutline,
    },
    {
        tab: 'profile',
        component: ProfilePage,
        exact: true,
        path: '/profile',
        label: 'profile',
        icon: radioButtonOff,
    },
]

interface CreateProjectProps {
    view: CREATE_VIEW
    users?: string[]
    locations?: string[]
    title?: string
    description?: string
    date?: ProjectDate
}

interface MessageProps {
    view: MESSAGES_PAGE
}

export interface Services {
    app: {
        users: User[]
        categories: Category[]
        messages: Message[]
        locations: ProjectLocation[]
        conversations: Conversation[]
        projects: Project[]
    }
}

// We will be using string literals and using typeof to declare our action constants and infer types. Note that we are making a tradeoff here when we declare our types in a separate file. In exchange for separating our types into a separate file, we get to keep our other files more focused on their purpose. While this tradeoff can improve the maintainability of the codebase, it is perfectly fine to organize your project however you see fit.
