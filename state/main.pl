:- use_module(library(http/http_open)).
:- use_module(library("http/json")).

folder("./state/files/users").
folder("./state/files/categories").
folder("./state/files/projects").

% %% CATEGORIES
categories(model).
categories(fotograph).
categories(artist).

% %% USER AND NPCS
% %% the currently logged in user
is_appuser(appuser).
user(appuser).

user(npc_one).
user(npc_two).
user(npc_three).
user(npc_four).
user(npc_five).
user(npc_six).
user(npc_seven).
user(npc_eight).
user(npc_nine).
user(npc_ten).
user(npc_eleven).
user(npc_twelve).

% %% USER ROLES
is_member_of_category(model, npc_one).
is_member_of_category(model, npc_two).
is_member_of_category(model, npc_three).
is_member_of_category(fotograph, npc_six).
is_member_of_category(fotograph, npc_one).
is_member_of_category(artist, npc_four).
is_member_of_category(artist, npc_five).
is_member_of_category(artist, npc_eight).

% %% PROJECTS
% weddings
project(wedding_one).
project(wedding_two).
project(wedding_three).
% birthdays
project(birthday_one).
project(birthday_two).
project(birthday_three).
% bachelor parties
project(bachelor_one).
project(bachelor_two).
% prom
project(prom_one).
project(prom_two).

% ownership
owns_project(wedding_one, npc_one).
owns_project(wedding_two, npc_two).
owns_project(wedding_three, appuser).

owns_project(birthday_one, appuser).
owns_project(birthday_two, npc_eight).
owns_project(birthday_three, npc_eight).

owns_project(bachelor_one, npc_eight).
owns_project(bachelor_two, npc_eight).

owns_project(prom_one, npc_eight).
owns_project(prom_two, appuser).

% projectmembers
works_on_project(wedding_one, npc_six).
works_on_project(wedding_one, npc_five).
works_on_project(wedding_one, appuser).
works_on_project(wedding_one, npc_three).
works_on_project(wedding_one, npc_eight).
works_on_project(wedding_one, npc_nine).
works_on_project(wedding_three, npc_four).
works_on_project(wedding_three, npc_eight).
works_on_project(wedding_three, npc_nine).

works_on_project(birthday_one, npc_four).
works_on_project(birthday_one, npc_eight).
works_on_project(birthday_one, npc_nine).
works_on_project(birthday_one, npc_one).
works_on_project(birthday_two, npc_three).
works_on_project(birthday_two, npc_one).
works_on_project(birthday_three, npc_three).
works_on_project(birthday_three, npc_one).
works_on_project(birthday_three, npc_four).
works_on_project(birthday_three, npc_nine).

works_on_project(bachelor_one, appuser).
works_on_project(bachelor_one, npc_five).
works_on_project(bachelor_one, npc_six).
works_on_project(bachelor_two, npc_one).
works_on_project(bachelor_two, npc_two).
works_on_project(bachelor_two, npc_nine).

works_on_project(prom_one, npc_one).
works_on_project(prom_one, npc_two).
works_on_project(prom_one, npc_nine).
works_on_project(prom_two, npc_seven).
works_on_project(prom_two, npc_six).
works_on_project(prom_two, npc_two).

category_all_members(X, Y) :-
    categories(X),
    is_member_of_category(X, Y).

project_uuid(X, ID) :-
    project(X),
    uuid(ID).

project_file(X, File) :-
    atom_concat("./state/files/projects/", X, F),
    atom_concat(F, ".json", File).

% %% GENERATOR PROJECT
project_generate_title(R) :-
    T = ['Wedding','Bachelor Party', 'Another Wedding', '60 Birthday', 'Prom'],
    random_member(R, T).

project_generate_description(R) :-
    T = ['My Wedding','Your Bachelor Party', 'Another Fancy Wedding', 'The Birthday of someone', 'Also a Birthday'],
    random_member(R, T).

project_generate(ID, P, F) :-
    open(F, append, Stream),
    project_generate_description(D),
    json_write(Stream,
        project{
            id: ID,
            title: P,
            description: D
        }, [tag(type)]),
    close(Stream).


category_uuid(X, ID) :-
    categories(X),
    uuid(ID).

category_file(X, File) :-
    atom_concat("./state/files/categories/", X, F),
    atom_concat(F, ".json", File).

category_generate(ID, C, F) :-
    open(F, append, Stream),
    json_write(Stream,
        category{
            id: ID,
            name: C
        }, [tag(type)]),
    close(Stream).

% user
user_uuid(X, ID) :-
    user(X),
    uuid(ID).

user_with_project(X, Y) :-
    project(X),
    owns_project(X, Y).

user_works_on_projects(X, Y) :-
    project(X),
    works_on_project(X, Y).

user_random_name(R) :-
    T = ['Harald','Eduard', 'Ernst', 'Hildrun', 'Barbara'],
    random_member(R, T).

user_file(X, File) :-
    atom_concat("./state/files/users/", X, F),
    atom_concat(F, ".json", File).

user_less_all :-
    user(X),
    user_file(X, File),
    atom_concat("less ", File, CMD),
    shell(CMD, S), write(S).

user_generate(ID, F) :-
    user_random_name(R),
    open(F, append, Stream),
    json_write(Stream,
        user{
            id: ID,
            name: R
        }, [tag(type)]),
    close(Stream).


% %% SETUP GENERATED STATE
setup_users :-
    user_uuid(X, ID),
    user_file(X, File),
    user_generate(ID, File).

setup_categories :-
    category_uuid(X, ID),
    category_file(X, File),
    category_generate(ID, X, File).

setup_projects :-
    project_uuid(X, ID),
    project_file(X, File),
    project_generate(ID, X, File).

% %% PREPEATE APP STATE
prepare_users :-
    shell("jq -s '[.[]]' ./state/files/users/*.json").

prepare_categories :-
    shell("jq -s '[.[]]' ./state/files/categories/*.json").

prepare_projects :-
    shell("jq -s '[.[]]' ./state/files/projects/*.json").

run :-
    setup_users,
    setup_categories.

% prepare_projects :-
%     project(X),
%     works_on_project(X, U),
%     shell("jq -s '[.[]]' ./state/files/projects/*.json", Output).

% %% CLEANUP
cleanup :-
    folder(X),
    atom_concat("rm -f ", X, Y),
    atom_concat(Y, "/*", CF),
    shell(CF).

start :- http_open('https://www.metalevel.at/prolog', Stream, []), write(Stream).
